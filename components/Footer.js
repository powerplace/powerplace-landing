import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Field, reduxForm, SubmissionError } from 'redux-form';
import { defineMessages, injectIntl, intlShape } from 'react-intl';

import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import Logo from '../static/icons/logo.svg';

import Input from './input/Input';
import validate from '../routes/landing/validate';

import Facebook from '../static/icons/social/facebook.svg';
import Instagram from '../static/icons/social/instagram.svg';
import Youtube from '../static/icons/social/youtube.svg';
import Linkedin from '../static/icons/social/linkedin.svg';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons";

const subscriptionCreateMutation = gql`
  mutation subscriptionCreate($email: String!) {
    subscriptionCreate(email: $email) {
      id
    }
  }
`;


const messages = defineMessages({
  emailPlaceholder: {
    id: 'landing.footer.email.placeholder',
    defaultMessage: 'E-Mail-Adresse',
  },
  buttonText: {
    id: 'landing.footer.email.button-text',
    defaultMessage: 'Join in',
  },
  emailSubmissionError: {
    id: 'landing.footer.email.submission-error',
    defaultMessage: 'Die E-Mail-Adresse konnte nicht gepeichert werden. Bitte versuchen Sie es später erneut.',
  },
  keepInformedSuccess: {
    id: 'landing.hero.keep-informed.success',
    defaultMessage: 'Sie haben sich erfolgreich registriert.',
  },
  dataProtection: {
    id: 'dataprotection.title',
    defaultMessage: 'Datenschutz'
  },
  home: {
    id: 'home',
    defaultMessage: 'Home'
  }
});

@graphql(subscriptionCreateMutation)
@injectIntl
@reduxForm({ form: 'footer', validate })
export default class Footer extends PureComponent {
  static propTypes = {
    intl: intlShape,
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
  }

  state = {
    success: false,
  }

  static defaultProps = {}

  onFormSubmit = async (values) => {
    const { mutate, intl: { formatMessage } } = this.props;

    try {
      await mutate({
        variables: { email: values.email },
      });
      this.setState({ success: true })
    } catch (e) {
      throw new SubmissionError({ email: formatMessage(messages.emailSubmissionError) });
    }
  }

  render() {
    const {
      handleSubmit, submitting, error, intl: { formatMessage },
    } = this.props;
    const { success } = this.state;

    return (
      <footer className="footer">
        <div className="footer__upper">
          <div className="footer__container">
            <div className="footer__row">
              <Logo viewBox="60 200 841.89 195.28" className="footer__logo" />
              <form onSubmit={handleSubmit(this.onFormSubmit)} noValidate>
                {
                  !success &&
                  <Field
                    component={Input}
                    name="email"
                    props={{
                      className: 'input--footer',
                      buttonText: formatMessage(messages.buttonText),
                      placeholder: formatMessage(messages.emailPlaceholder),
                    }}
                  />
                }
                {success &&
                  <div className="landing__keep-informed__success" style={{color: 'white'}}>
                    <FontAwesomeIcon icon={faCheckCircle} size='lg'/>
                    <span style={{marginLeft: '5px'}}>{formatMessage(messages.keepInformedSuccess)}</span>
                  </div>
                }
              </form>
            </div>
            <div className="footer__row">
              <a href="mailto:contact@powerplace.ch" className="footer__email">contact@powerplace.ch</a>
              <div className="footer__social">
                <a href="https://www.facebook.com/PowerPlaceEnergy" className="footer__social__item">
                  <Facebook className="footer__social__icon" />
                </a>
                <a href="https://www.instagram.com/powerplaceenergy" className="footer__social__item">
                  <Instagram className="footer__social__icon" />
                </a>
                <a href="https://www.youtube.com/channel/UCeRTAaiPJdEhYMo8VoaEeVg/featured" className="footer__social__item">
                  <Youtube className="footer__social__icon" />
                </a>
                <a href="https://www.linkedin.com/company/powerplaceenergy" className="footer__social__item">
                  <Linkedin className="footer__social__icon" />
                </a>
              </div>
            </div>
            <div className="footer__row">
              <a href='/' className="footer__data-protection">{formatMessage(messages.home)}</a>
              <a href='/data-protection' className="footer__data-protection">{formatMessage(messages.dataProtection)}</a>
            </div>
          </div>
        </div>
        <div className="footer__lower">
          <div className="footer__container">
            {new Date().getFullYear()} Powerplace. All rights reserved.
          </div>
        </div>
      </footer>
    );
  }
}
