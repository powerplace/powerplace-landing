import React, { PureComponent } from 'react'
import Cookies from 'js-cookie'
import Logo from '../static/icons/logo.svg'
import Youtube from '../static/icons/social/youtube.svg'

import Facebook from '../static/icons/social/facebook.svg'
import Instagram from '../static/icons/social/instagram.svg'
import Linkedin from '../static/icons/social/linkedin.svg'

export default class Header extends PureComponent {
  static propTypes = {}

  static defaultProps = {}

  onLanguageClick = (language) => {
    Cookies.set('powerplace-language', language);
    window.location.reload();
  }

  render() {

    return (
      <header className="header">
        <div className="header__content">
          <ul className="header__language">
            <button
              className="header__language__item"
              onClick={() => {
                this.onLanguageClick('de')
              }}
            >
              DE
            </button>
            <button
              className="header__language__item"
              onClick={() => {
                this.onLanguageClick('fr')
              }}
            >
              FR
            </button>
            <button
              className="header__language__item"
              onClick={() => {
                this.onLanguageClick('it')
              }}
            >
              IT
            </button>
          </ul>
          <Logo viewBox="0 200 841.89 195.28" className="header__logo" />
          <div className="header__social">
            <a href="https://www.facebook.com/PowerPlaceEnergy" className="header__social__item">
              <Facebook className="header__social__icon" />
            </a>
            <a href="https://www.instagram.com/powerplaceenergy" className="header__social__item">
              <Instagram className="header__social__icon" />
            </a>
            <a href="https://www.youtube.com/channel/UCeRTAaiPJdEhYMo8VoaEeVg/featured"
               className="header__social__item">
              <Youtube className="header__social__icon" />
            </a>
            <a href="https://www.linkedin.com/company/powerplaceenergy" className="header__social__item">
              <Linkedin className="header__social__icon" />
            </a>
          </div>
        </div>

      </header>
    )
  }
}
