import Head from 'next/head';
import NProgress from 'nprogress';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import LiveChat from 'react-livechat';
import Cookies from 'js-cookie'
import {
  defineMessages,
  injectIntl,
  intlShape,
} from 'react-intl';
import config from '../config';
import { Router } from '../routes';

import stylesheet from '../styles/styles.scss';
import Footer from './Footer';

import Header from './Header';

const messages = defineMessages({
  title: {
    id: 'app.title',
    defaultMessage: 'Powerplace',
  },
  appName: {
    id: 'app.name',
    defaultMessage: 'Powerplace',
  },
});

const localeToGroup = {
  'de': 0,
  'fr': 1,
  'it': 3,
};

Router.onRouteChangeStart = () => NProgress.start();
Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();

@injectIntl
class Layout extends Component {
  static propTypes = {
    title: PropTypes.string,
    children: PropTypes.any,
    intl: intlShape,
  };

  render() {
    const { title, children, intl: { formatMessage } } = this.props;
    return (
      <section className="site">
        <Head>
          <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
          <title>{title} | {formatMessage(messages.title)}</title>
          <meta charSet="utf-8" />
          <meta name="keywords" content="Heizöl, Heizölpreis, günstig, Preisvergleich, Oelmarkt, Benzin, Diesel, Treibstoff, Brennstoff, Heizöl bestellen, Heizöl günstig, Heizöl billig, Heizöl kaufen, preiswert Heizöl kaufen, Preise vergleichen, Heizöl online bestellen" />
          <meta name="description" content="" />
          <meta name="viewport" content="initial-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta name="mobile-web-app-capable" content="yes" />
          <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
          <meta name="format-detection" content="telephone=no" />
          <meta name="apple-mobile-web-app-title" content={formatMessage(messages.appName)} />
          <meta name="msvalidate.01" content="93AA44E8A73BFE425C8A2FB6DF9706EC" />
          <meta name="google-site-verification" content="RrSp0JMtHOsgcI2hUvyWIk7-delRyy4QGk138Zka75U" />


          <link rel="icon" href={`${config.basePath}/static/favicon.ico?v=1`} />
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400" rel="stylesheet" />

        </Head>
        <Header />
        <main className="site__content">
          {children}
        </main>
        <Footer />
        { process.browser && (
          <LiveChat
            license={config.livechatLicense}
            group={localeToGroup[Cookies.get('powerplace-language')]}
          />
        )}
      </section>
    );
  }
}

export default Layout;
