import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class HeroContent extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
  }

  static defaultProps = {
    className: null,
  }

  render() {
    const { children, className, ...props } = this.props;

    const heroDescriptionClasses = classnames('hero__content', className);

    return (
      <div className={heroDescriptionClasses} {...props}>
        {children}
      </div>
    );
  }
}

