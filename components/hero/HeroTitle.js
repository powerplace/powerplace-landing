import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Hero extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
  }

  static defaultProps = {
    className: null,
  }

  render() {
    const { children, className, ...props } = this.props;

    const heroTitleClasses = classnames('hero__title', className);

    return (
      <h1 className={heroTitleClasses} {...props}>
        {children}
      </h1>
    );
  }
}

