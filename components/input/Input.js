import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Input extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    placeholder: PropTypes.string,
    buttonText: PropTypes.string,
    onButtonClicked: PropTypes.func.isRequired,
  }

  static defaultProps = {
    className: null,
    placeholder: null,
    buttonText: null,
  }

  render() {
    const {
      placeholder, className, buttonText, input, meta, onButtonClicked,
    } = this.props;

    const inputClasses = classnames('input', className, {
      'input--error': meta.error && meta.touched,
    });

    return [
      <div key="input-wrapper" className="input-wrapper">
        <input className={inputClasses} {...input} placeholder={placeholder} type="email" />
        <button type="submit" className="input__button" onClick={onButtonClicked}>{buttonText}</button>
      </div>,
      <div key="input-error">
        {meta.touched && meta.error &&
        <div className="input__error">{meta.error}</div>}
      </div>,
    ];
  }
}

