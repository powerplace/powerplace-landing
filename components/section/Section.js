import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Section extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    wizard: PropTypes.bool,
  }

  static defaultProps = {
    className: null,
    wizard: false,
  }

  render() {
    const {
      children, className, wizard, ...props
    } = this.props;

    const sectionClasses = classnames('section', className, {
      'section--wizard': wizard,
    });

    return (
      <section className={sectionClasses} {...props}>
        {children}
      </section>
    );
  }
}

