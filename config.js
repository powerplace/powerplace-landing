export default {
  NOW_URL: process.env.NOW_URL || 'http://localhost:3000',
  basePath: process.env.BASE_PATH || '',
  client: process.env.CLIENT === 'true',
  httpProxy: process.env.HTTP_PROXY || '',
  nodeEnv: process.env.NODE_ENV || '',
  port: (parseInt(process.env.PORT, 10) || 3000),
  server: process.env.SERVER === 'true',
  sslCertificate: process.env.SSL_CERTIFICATE || '',
  sslCertificateKey: process.env.SSL_CERTIFICATE_KEY || '',
  livechatLicense: process.env.LIVECHAT_LICENSE || 11132467
};
