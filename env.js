const env = {
  APP_ID: 'Powerplace',
  PORT: '3000',
  LOG_LEVEL: 'debug',
  VERIFICATION_JWT_SECRET: 'powerplace_secret_abcd.1234',
  MONGO_URL: 'mongodb://powerplace:abcd.1234@ds231987.mlab.com:31987/powerplace-subscriptions',
  MONGOOSE_DEBUG: true,
};

export default env;
