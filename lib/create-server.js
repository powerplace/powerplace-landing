import fs from 'fs';
import http from 'http';
import https from 'https';

export default function createServer({ cert, key }, app) {
  let ssl = null;

  // read SSL cert & key if available
  if (cert && key) {
    try {
      ssl = {
        cert: fs.readFileSync(cert, 'utf8'),
        key: fs.readFileSync(key, 'utf8'),
      };
    } catch (err) {
      ssl = null;
    }
  }

  if (ssl) {
    try {
      console.log('Creating HTTPS server.'); // eslint-disable-line no-console
      return https.createServer({ cert: ssl.cert, key: ssl.key }, app);
    } catch (err) {
      // fall back to HTTP
      console.log('Falling back to HTTP server.'); // eslint-disable-line no-console
      return http.createServer(app);
    }
  }

  // default to HTTP
  console.log('Defaulting to HTTP server.'); // eslint-disable-line no-console
  return http.createServer(app);
}
