import { readFileSync } from 'fs';

const localeDataCache = new Map();

export function getLocaleDataScript(locale) {
  const lang = locale.split('-')[0];

  if (!localeDataCache.has(lang)) {
    const localeDataFile = require.resolve(`react-intl/locale-data/${lang}`);
    const localeDataScript = readFileSync(localeDataFile, 'utf8');

    localeDataCache.set(lang, localeDataScript);
  }

  return localeDataCache.get(lang);
}

export function getLocaleMessages(locale) {
  return require(`../lang/${locale}.json`); // eslint-disable-line
}
