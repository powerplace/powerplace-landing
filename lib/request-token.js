
import queryString from 'query-string';
import config from '../config';

export async function requestTokenByCode(code, agent) {
  // prepare client credentials as basic auth for development only
  const basic = Buffer.from(`${config.oauthClientId}:${config.oauthClientSecret}`).toString('base64');

  const response = await fetch(config.oauthTokenEndpoint, {
    agent,
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
      ...(config.nodeEnv !== 'production' ? {
        Authorization: `Basic ${basic}`,
      } : {}),
    },
    body: queryString.stringify({
      code,
      grant_type: 'authorization_code',
      client_id: config.oauthClientId,
      client_secret: config.oauthClientSecret,
      redirect_uri: config.oauthRedirectUri,
    }),
  });

  const token = await response.json();

  if (token.error) {
    throw new Error(`Failed to obtain access token: "${token.error_description}"`);
  }

  return {
    accessToken: token.access_token,
    tokenType: 'bearer',
    refreshToken: token.refresh_token,
    expiresIn: parseInt(token.expires_in, 10),
    scope: token.scope,
  };
}
