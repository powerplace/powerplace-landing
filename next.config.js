const { DefinePlugin, IgnorePlugin } = require('webpack'); // eslint-disable-line
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer'); // eslint-disable-line
const path = require('path');
const glob = require('glob');

const { ANALYZE } = process.env;

module.exports = {
  assetPrefix: `${process.env.BASE_PATH || ''}/`,
  webpack: config => ({
    ...config,
    module: {
      ...config.module,
      rules: [
        ...config.module.rules,
        {
          test: /\.(css|scss)/,
          loader: 'emit-file-loader',
          options: {
            name: 'dist/[path][name].[ext]',
          },
        },
        {
          test: /\.css$/,
          use: ['babel-loader', 'raw-loader', 'postcss-loader'],
        },
        {
          test: /\.s(a|c)ss$/,
          use: ['babel-loader', 'raw-loader', 'postcss-loader',
            {
              loader: 'sass-loader',
              options: {
                includePaths: ['styles', 'node_modules']
                  .map(d => path.join(__dirname, d))
                  .map(g => glob.sync(g))
                  .reduce((a, c) => a.concat(c), []),
              },
            },
          ],
        },
        /*  {
            test: /\.js$/,
            loader: 'eslint-loader',
            enforce: 'pre',
            exclude: /node_modules/,
          }, */
      ],
    },
    plugins: [
      new IgnorePlugin(/https-proxy-agent/),
      new DefinePlugin({
        'process.env': 'window.process.env',
      }),
      ...config.plugins,
      ...(ANALYZE ? [
        new BundleAnalyzerPlugin({
          analyzerMode: 'server',
          analyzerPort: 8888,
          openAnalyzer: true,
        }),
      ] : []),
    ],
  }),
};
