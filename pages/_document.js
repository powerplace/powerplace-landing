import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';

const GA_TRACKING_ID = 'UA-113138245-1';

export default class CustomDocument extends Document {
  static async getInitialProps(context) {
    const props = await super.getInitialProps(context);
    const { req: { env, locale, localeDataScript } } = context;

    return {
      ...props,
      env,
      locale,
      localeDataScript,
    };
  }

  render() {
    return (
      <html lang={this.props.locale}>
        <Head>
          <meta http-equiv="X-UA-Compatible" content="IE=edge" />
          <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css"></link>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.23.0/polyfill.min.js" />
          <script async src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`} />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments)};
                gtag('js', new Date());
                gtag('config', '${GA_TRACKING_ID}');
              `,
            }}
          />
        </Head>
        <body>
          <Main />
          <script
            src={`https://cdn.polyfill.io/v2/polyfill.min.js?features=Intl.~locale.${this.props.locale}`}
          />
          <script
            dangerouslySetInnerHTML={{ // eslint-disable-line
              __html: `
                ${this.props.localeDataScript}
                window.process = {};
                window.process.env = ${JSON.stringify({
                  BASE_PATH: process.env.BASE_PATH,
                    NOW_URL: process.env.NOW_URL || 'http://localhost:3000',
                })};
                window.process.env.CLIENT = 'true';
                window.process.env.SERVER = 'false';
              `,
            }}
          />
          <NextScript />
        </body>
      </html>
    );
  }
}
