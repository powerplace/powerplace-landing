import React, { Component } from 'react';
import withIntl from './with/withIntl';

@withIntl
export default class Error extends Component {
  render() {
    return (
      <div>
        <p>Page not found</p>
      </div>
    );
  }
}
