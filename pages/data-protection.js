import React, { PureComponent } from 'react';
import Head from 'next/head';
import stylesheet from '../styles/routes/data-protection.scss';
import Layout from '../components/Layout';
import withIntl from "./with/withIntl";
import withApollo from "./with/withApollo";
import withRedux from "next-redux-wrapper";
import { initStore } from '../store/index';
import {defineMessages, intlShape} from "react-intl";

const messages = defineMessages({
  title: { id: 'dataprotection.title', defaultMessage: 'Datenschutz' },
  subtitle: { id: 'dataprotection.subtitle', defaultMessage: 'DATENSCHUTZHINWEIS' },
  content: {
    id: 'dataprotection.content',
    defaultMessage: `Der Schutz Ihrer Privatsphäre ist uns ein wichtiges Anliegen.
      Soweit auf dieser Website personenbezogene Daten erhoben werden wie Name, Anschrift oder E-Mail-Adresse, erfolgt
      dies auf freiwilliger Basis. Ihre Daten werden vertraulich behandelt und nicht an Dritte weitergegeben. In 
      Zusammenarbeit mit unseren Hosting-Providern bemühen wir uns, die Datenbanken so gut wie möglich vor fremden
      Zugriffen, Verlusten, Missbrauch oder vor Fälschung zu schützen. Beim Zugriff auf diese Website werden folgende
      Daten in Logfiles gespeichert: IP-Adresse, Datum, Uhrzeit, Browser-Anfrage und allgemeine Informationen zum
      Betriebssystem respektive zum verwendeten Browser. Im Weiteren setzen wir Webanalyse-Dienste ein, die sog. Cookies
      auf Ihrem Computer speichern. Nähere Angaben zu den eingesetzten Diensten sowie Informationen, wie Sie diese bei
      Bedarf deaktivieren können, finden Sie weiter unten im Text. Alle Nutzungsdaten werden ausschließlich für
      statistische, anonyme Auswertungen genutzt, die uns helfen, diese Website zu optimieren. Darüber hinaus sind bei
      dieser Website Social-Media-Kanäle wie z.B. Facebook / Instagram / YouTube / LinkedIn / Twitter eingebunden. Für
      Angaben zu den Datenschutzrichtlinien verweisen wir auf die jeweiligen Websites.`
  }
});

@withIntl
@withApollo
@withRedux(initStore)
export default class Index extends PureComponent {
  static propTypes = {
    intl: intlShape,
  }

  static getInitialProps({ store }) {
  }

  render() {
    const { intl: { formatMessage } } = this.props;
    console.log(formatMessage(messages.title));

    return (
      <Layout title={formatMessage(messages.title)}>
        <div>
          <Head>
            <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
          </Head>
          <div className="data-protection">
            <h1 className="data-protection__header">{formatMessage(messages.title)}</h1>
            <div className="data-protection__body">
              <div className="data-protection__body__container">
                <h2 className="data-protection__body__container__title">{formatMessage(messages.subtitle)}</h2>
                <p className="data-protection__body__container__description">{formatMessage(messages.content)}</p>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}
