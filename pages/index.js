

import React, { PureComponent } from 'react';
import { defineMessages, intlShape } from 'react-intl';

import withRedux from 'next-redux-wrapper';
import { initStore } from '../store/index';

import withIntl from './with/withIntl';
import withApollo from './with/withApollo';

import Layout from '../components/Layout';
import Landing from '../routes/landing/Landing';

const messages = defineMessages({
  title: { id: 'landing.title', defaultMessage: 'Die intelligenteste Plattform für Heizöl' },
});

@withIntl
@withApollo
@withRedux(initStore)
export default class Index extends PureComponent {
  static propTypes = {
    intl: intlShape,
  }

  static getInitialProps({ store }) {
  }

  render() {
    const { intl: { formatMessage } } = this.props;

    return (
      <Layout title={formatMessage(messages.title)}>
        <Landing />
      </Layout>
    );
  }
}
