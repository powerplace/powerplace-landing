import React, { Component } from 'react';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Head from 'next/head';
import withApollo from "./with/withApollo";
import stylesheet from '../styles/routes/subscribed-users.scss';

const QUERY_ALL_SUBSCRIPTIONS = gql`
  query QUERY_ALL_SUBSCRIPTIONS {
    subscriptionFindMany {
      created_at
      email
    }
  }
`;

const REMOVE_USER_MUTATION = gql`
  mutation REMOVE_USER_MUTATION($email: String!) {
    subscriptionRemove(email: $email) {
      id
    }
  }
`;


@withApollo
class SubscribedUsersPage extends Component {
  state = {
    toggleDaily: false
  }

  renderSub = subscriber => (
    <Mutation
      mutation={REMOVE_USER_MUTATION}
      variables={{email: subscriber.email}}
      refetchQueries={() => [{
        query: QUERY_ALL_SUBSCRIPTIONS
      }]}
      key={subscriber.email}
    >
      {(removeUser, {error, loading}) => {
        return (
          <div className="subscribed-users__body__item">
            <div>{ subscriber.email }</div>
            { error ? <div> Error... </div> :
              loading ? <div> Loading... </div> : (
                <button className="ui red button" type="button" onClick={() => removeUser(subscriber.email)}>
                  Unsubscribe
                </button>
              )
            }
          </div>
        );
      }}
    </Mutation>
  );

  filterSub = subscriber => {
    const date = new Date().setHours(0,0,0, 0);
    const subDate = new Date(subscriber.created_at).setHours(0,0,0,0);

    return this.state.toggleDaily ? date === subDate : true
  }

  toggleDaily = () => {
    this.setState({
      toggleDaily: !this.state.toggleDaily
    })
  }

  render() {
    return (
      <div>
        <Head>
          <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
        </Head>
        <Query query={QUERY_ALL_SUBSCRIPTIONS}>
          { ({data, error, loading}) => {
            if (error) return <div> Error... </div>;
            if (loading)return <div> Loading... </div>;

            const dataToRender =  data.subscriptionFindMany
              .filter(this.filterSub)
              .map(this.renderSub);

            return (
              <div className="subscribed-users">
                <div className="subscribed-users__header">
                  <div> Email </div>
                  <button type="button" className="ui button" onClick={this.toggleDaily}>
                    {this.state.toggleDaily ? 'Daily' : 'All'}
                  </button>
                </div>

                <div className="subscribed-users__body">
                  { dataToRender.length > 0 ? dataToRender : <div className="subscribed-users__body__no-data"> No entry... </div> }
                </div>
              </div>
            )
          }}
        </Query>
      </div>
    );
  }
}

export default SubscribedUsersPage;
