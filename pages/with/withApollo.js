import { withData } from 'next-apollo';
import { HttpLink } from 'apollo-link-http';

const config = {
  link: new HttpLink({
    uri: '/api/graphql', // Server URL (must be absolute)
    credentials: 'same-origin',
  }),
};

export default withData(config);
