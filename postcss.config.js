const postcssEasyImport = require('postcss-easy-import');
const autoprefixer = require('autoprefixer');
const flexbugsfixer = require('postcss-flexbugs-fixes');

module.exports = {
  plugins: [
    postcssEasyImport({ prefix: '_' }), // keep this first
    autoprefixer({ /* ...options */ }), // so imports are auto-prefixed too
    flexbugsfixer({ /* ...options */ }), // so imports are auto-prefixed too
  ],
};
