import { createAction } from 'redux-actions';
import { pathOr, assocPath } from 'ramda';

import nameActions from './nameActions';

export const ERROR_CODE = {
  FETCH_THREW_ERROR: 'FETCH_THREW_ERROR',
  INVALID_RESPONSE_STATUS: 'INVALID_RESPONSE_STATUS',
  JSON_THREW_ERROR: 'JSON_THREW_ERROR',
  BAD_REQUEST: 'BAD_REQUEST', // Srv:(400, "The request could not be understood by the server due to malformed syntax"),
  MALFORMED_JSON_PARAM: 'MALFORMED_JSON_PARAM', // Server: (400, "A JSON param could not be parsed"),
  ACCESS_DENIED: 'ACCESS_DENIED', // Server: (403, "Access to the resource was denied"),
  RESOURCE_NOT_FOUND: 'RESOURCE_NOT_FOUND', // Server: (404, "There is no resource at the requested path"),
  NOT_FOUND: 'NOT_FOUND', // Server: (404, "The requested resource was not found"),
  METHOD_NOT_ALLOWED: 'METHOD_NOT_ALLOWED', // Server: (405, "Method not allowed"),
  UNSUPPORTED_CONTENT_TYPE: 'UNSUPPORTED_CONTENT_TYPE', // Server: (406, "The content-type request is not supported"),
  NOT_ACCEPTABLE: 'NOT_ACCEPTABLE', // Server: (406, "The request is not acceptable"),
  VALIDATION_ERROR: 'VALIDATION_ERROR', // Server: (422, "The request could not be validated successfully"),
  UNEXPECTED_ERROR: 'UNEXPECTED_ERROR', // Server: (500, "An unexpected error occured");
};

export const FETCH_STATUS = {
  LOADING: 'loading',
  ERROR: 'error',
  LOADED: 'loaded',
};

export const API_ERROR_CODES = {
  NOT_FOUND: 'NOT_FOUND', // is different to RESOURCE_NOT_FOUND where there is no resource known at the requested path
};

export default function createApiActions(prefix, namespace, {
  url: urlArg,
  requestOptions: requestOptionsArg,
}) {
  const namedActions = nameActions('poseidon')(namespace);
  const start = createAction(namedActions(`${prefix}_START`));
  const success = createAction(namedActions(`${prefix}_SUCCESS`));
  const error = createAction(namedActions(`${prefix}_ERROR`));

  const action = payload => async (dispatch, getState) => {
    const { global } = getState();
    const { apiUrl, currentLanguage } = global;

    const defaultRequestOptions = {
      headers: new Headers({
        'Accept-Language': currentLanguage.key,
        'Content-Type': 'application/json;charset=UTF-8',
      }),
    };
    const startTime = new Date().getTime();

    dispatch(start(payload));

    let url = urlArg;
    let requestOptions = requestOptionsArg;

    if (typeof url === 'function') {
      url = url(payload);
    }

    if (typeof requestOptions === 'function') {
      requestOptions = requestOptions(payload);
    }


    requestOptions = Object.assign(defaultRequestOptions, requestOptions);

    requestOptions = assocPath(
      ['credentials'],
      'same-origin',
      requestOptions || {},
    );

    let response;
    try {
      response = await fetch(`${apiUrl}${url}`, requestOptions);
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e);

      const err = {
        error: ERROR_CODE.FETCH_THREW_ERROR,
        payload,
      };

      dispatch(error(err));

      throw err;
    }

    if (response.status < 200 || response.status > 299) {
      // eslint-disable-next-line no-console
      console.log(response.status, url);

      let body = null;

      const contentType = response.headers.get('content-type');

      // extract json if it is available
      if (contentType && contentType.toLowerCase().indexOf('application/json') !== -1) {
        try {
          body = await response.json();
        } catch (e) {
          console.log('Could not parse response body', e) // eslint-disable-line
        }
      }

      const err = {
        body,
        error: pathOr(ERROR_CODE.INVALID_RESPONSE_STATUS, ['error', 'code'], body),
        responseStatus: response.status,
        payload,
      };

      dispatch(error(err));

      throw err;
    }

    let body;

    const contentType = response.headers.get('content-type');

    // extract json if it is available
    if (contentType && contentType.toLowerCase().indexOf('application/json') !== -1) {
      try {
        body = await response.json();
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e);

        const err = {
          error: ERROR_CODE.JSON_THREW_ERROR,
          payload,
        };

        dispatch(error(err));

        throw err;
      }
    }

    dispatch(success({ payload, response, body }));

    const endTime = new Date().getTime();

    // eslint-disable-next-line no-console
    console.log(`API call to ${url} took ${endTime - startTime}ms`);

    return body;
  };

  return {
    action,
    start,
    error,
    success,
  };
}
