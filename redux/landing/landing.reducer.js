import { handleActions } from 'redux-actions';
import {
  toggleCompanyRegistration,
} from './landing.actions';

const loginDefaultState = {
  isCompanyRegistrationActive: false,
  loading: false,
  success: false,
  error: false,
};

const loginReducer = handleActions(
  {
    [toggleCompanyRegistration]: state =>
      ({
        ...state,
        isCompanyRegistrationActive: !state.isCompanyRegistrationActive,
      }),
  },
  loginDefaultState,
);

export default loginReducer;
