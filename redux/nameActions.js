const nameActions = app => domain => action => `${app}/${domain}/${action.toUpperCase()}`;

export default nameActions;
