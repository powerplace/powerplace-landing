import nextRoutes from 'next-routes';
import config from './config';

const routes = nextRoutes(); // eslint-disable-line no-multi-assign
const { basePath } = config;

routes.add('home', `${basePath}/`, 'index');
routes.add('login', `${basePath}/login`, 'login');
routes.add('dashboard', `${basePath}/dashboard`, 'dashboard');
routes.add('admin', `${basePath}/admin`, 'admin');
routes.add('admin-users', `${basePath}/admin/users`, 'admin-users');
routes.add('admin-companies', `${basePath}/admin/companies`, 'admin-companies');
routes.add('data-protection', `${basePath}/data-protection`, 'data-protection');

module.exports = routes;
