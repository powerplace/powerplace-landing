import React, { PureComponent } from 'react'
import { injectIntl, intlShape } from 'react-intl'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Field, reduxForm, SubmissionError } from 'redux-form'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'

import Section from '../../components/section/Section'

import validate from './validate'
import messages from './messages'
import Hero from '../../components/hero/Hero'
import HeroTitle from '../../components/hero/HeroTitle'
import HeroDescription from '../../components/hero/HeroDescription'
import HeroContent from '../../components/hero/HeroContent'
import Input from '../../components/input/Input'

import Compare from '../../static/icons/steps/compare.svg'
import Choose from '../../static/icons/steps/choose.svg'
import Order from '../../static/icons/steps/order.svg'
import Delivery from '../../static/icons/steps/delivery.svg'
import Youtube from '../../static/icons/social/youtube.svg'
import Facebook from '../../static/icons/social/facebook.svg'
import Instagram from '../../static/icons/social/instagram.svg'
import Linkedin from '../../static/icons/social/linkedin.svg'

const subscriptionCreateMutation = gql`
  mutation subscriptionCreate($email: String!) {
    subscriptionCreate(email: $email) {
      id
    }
  }
`


@injectIntl
@connect(
  ({ landing }) => ({ landing }),
  null,
)
@reduxForm({
  form: 'landing',
  validate,
})
@graphql(subscriptionCreateMutation)
export default class Landing extends PureComponent {
  static propTypes = {
    intl: intlShape,
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
  }

  static defaultProps = {}

  state = {
    success: false,
    isPlaying: false,
  }


  onFormSubmit = async (values, event) => {
    console.log(event)
    const { mutate, intl: { formatMessage, locale } } = this.props

    try {
      await mutate({
        variables: { email: values.email },
      })
      this.setState({ success: true })
    } catch (e) {
      if (e.graphQLErrors && e.graphQLErrors[0] && e.graphQLErrors[0].name === 'EmailAlreadyInUse') {
        throw new SubmissionError({ email: formatMessage(messages.emailAlreadyInUseSubmissionError) })
      }
      throw new SubmissionError({ email: formatMessage(messages.emailSubmissionError) })
    }
  }
  handleVideoClicked = (event) => {
    event.preventDefault();
    console.log('click')
    const { isPlaying } = this.state
    this.setState({ isPlaying: !isPlaying })
    if (!isPlaying) {
      this.video.play()
    } else {
      this.video.pause()
    }
  }

  render() {
    const {
      handleSubmit, submitting, error, intl: { formatMessage, locale },
    } = this.props

    const { success, isPlaying }
      = this.state

    return (
      <Section className="landing">
        <div className="landing__hero-wrapper">
          <div className="landing__container landing__container--50vh">
            <Hero>
              <HeroTitle>
                {formatMessage(messages.heroTitle)}
              </HeroTitle>
              <HeroDescription>
                {formatMessage(messages.heroDescription)}
              </HeroDescription>
              <HeroContent>
                <h2 className="landing__countdown">
                  {formatMessage(messages.goLive)}
                </h2>
                <div className="landing__keep-informed">
                  <p className="landing__keep-informed__title">
                    {formatMessage(messages.keepInformed)}
                  </p>
                  <form onSubmit={handleSubmit(this.onFormSubmit)} noValidate>
                    {
                      !success &&
                      <Field
                        name="email"
                        component={Input}
                        props={{
                          buttonText: formatMessage(messages.buttonText),
                          placeholder: formatMessage(messages.emailPlaceholder),
                        }}
                      />
                    }
                    {success &&
                      <div className="landing__keep-informed__success">
                        <FontAwesomeIcon icon={faCheckCircle} size='lg'/>
                        <span style={{marginLeft: '5px'}}>{formatMessage(messages.keepInformedSuccess)}</span>
                      </div>
                    }
                  </form>
                </div>

              </HeroContent>
            </Hero>
          </div>
        </div>
        <div className="landing__to-video">
          <a className="landing__to-video__link" href="#powerplace-video"> {formatMessage(messages.toVideo)}</a>
        </div>
        <Section className="landing__section landing__section--white">
          <h1 className="landing__video-title">
            {formatMessage(messages.videoTitle)}
          </h1>
          <div className="landing__container landing__container--video">
            <div className="landing__video-overlay" onClick={this.handleVideoClicked}>
              {!isPlaying &&
              <svg className="landing__video-overlay__play-button" viewBox="0 0 200 200" alt="Play video">
                <circle className="landing__video-overlay__play-button__circle" cx="100" cy="100" r="90" fill="#212121"
                        fillOpacity="0.8" strokeWidth="15" stroke="white" />
                <polygon points="70, 55 70, 145 145, 100" fill="white" />
              </svg>
              }

              <video
                ref={(video) => {
                  this.video = video
                }}
                controls={isPlaying}
                poster="/static/images/video.jpg"
                id="powerplace-video"
                className="landing__video"
              >
                <source src={`/static/videos/${locale.split('-')[0].toUpperCase()}_PowerPlace_video.mp4`} type="video/mp4" />
              </video>

            </div>

            <div className="landing__social">
              <a href="https://www.facebook.com/PowerPlaceEnergy" className="landing__social__item">
                <Facebook className="landing__social__icon" />
              </a>
              <a href="https://www.instagram.com/powerplaceenergy" className="landing__social__item">
                <Instagram className="landing__social__icon" />
              </a>
              <a
                href="https://www.youtube.com/channel/UCeRTAaiPJdEhYMo8VoaEeVg/featured"
                className="landing__social__item"
              >
                <Youtube className="landing__social__icon" />
              </a>
              <a
                href="https://www.linkedin.com/company/powerplaceenergy"
                className="landing__social__item"
              >
                <Linkedin className="landing__social__icon" />
              </a>
            </div>
          </div>
        </Section>
        < Section
          className='landing__section landing__section--gray'>
          < h1
            className='landing__steps-title'>
            {formatMessage(messages.stepsTitle,
            )
            }
          </h1>

          <div className="landing__container landing__container--steps">
            <ul className="landing__steps">
              <li className="landing__steps__item">
                <Compare className="landing__step__icon" />
                <p className="landing__step__text">{formatMessage(messages.stepCompare)}</p>
              </li>
              <li className="landing__steps__item">
                <Choose className="landing__step__icon" />
                <p className="landing__step__text">{formatMessage(messages.stepChoose)}</p>
              </li>
              <li className="landing__steps__item">
                <Order className="landing__step__icon" />
                <p className="landing__step__text">{formatMessage(messages.stepOrder)}</p>
              </li>
              <li className="landing__steps__item">
                <Delivery className="landing__step__icon" />
                <p className="landing__step__text">{formatMessage(messages.stepDelivery)}</p>
              </li>
            </ul>
            <h2 className='landing__steps-summary'>
              {formatMessage(messages.stepsSummary)}
            </h2>

          </div>

        </Section>
      </Section>
    )
  }
}

