import { defineMessages } from 'react-intl';

const messages = defineMessages({
  heroTitle: {
    id: 'landing.hero.title',
    defaultMessage: 'Die Smarteste Plattform für Heizöl',
  },
  heroDescription: {
    id: 'landing.hero.description',
    defaultMessage: 'Vergleiche die Preise und wähle die beste Lösung für deine Bedürfnisse',
  },
  goLive: {
    id: 'landing.hero.go-live',
    defaultMessage: 'Geplanter Go-Live Termin: Juni 2018',
  },
  keepInformed: {
    id: 'landing.hero.keep-informed',
    defaultMessage: 'Halte dich auf dem Laufenden!',
  },
  keepInformedSuccess: {
    id: 'landing.hero.keep-informed.success',
    defaultMessage: 'Sie haben sich erfolgreich registriert.',
  },
  emailPlaceholder: {
    id: 'landing.hero.email.placeholder',
    defaultMessage: 'E-Mail-Adresse',
  },
  emailSubmissionError: {
    id: 'landing.hero.email.submission-error',
    defaultMessage: 'Die E-Mail-Adresse konnte nicht gepeichert werden. Bitte versuchen Sie es später erneut.',
  },
  emailAlreadyInUseSubmissionError: {
    id: 'landing.hero.keep-informed.error',
    defaultMessage: 'Ihre E-Mail-Adresse ist bereits registriert.',
  },
  buttonText: {
    id: 'landing.hero.email.button-text',
    defaultMessage: 'Join in',
  },
  toVideo: {
    id: 'landing.hero.to-video',
    defaultMessage: 'Zum Video gehen',
  },
  videoTitle: {
    id: 'landing.video-title',
    defaultMessage: 'Es ist einfach, schnell und sicher...',
  },
  stepsTitle: {
    id: 'landing.steps-title',
    defaultMessage: 'Alles in 4 Schritten',
  },
  stepsSummary: {
    id: 'landing.steps-summary',
    defaultMessage: 'PowerPlace, wir erwärmen nicht nur dein Haus, sondern auch dein Herz!',
  },
  stepCompare: {
    id: 'landing.step-compare',
    defaultMessage: 'Vergleichen',
  },
  stepChoose: {
    id: 'landing.step-choose',
    defaultMessage: 'Wählen',
  },
  stepOrder: {
    id: 'landing.step-order',
    defaultMessage: 'Bestellen',
  },
  stepDelivery: {
    id: 'landing.step-delivery',
    defaultMessage: 'Lieferung',
  },
});

export default messages;
