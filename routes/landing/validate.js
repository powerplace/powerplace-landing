import React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import { required, email } from 'redux-form-validators';

const messages = defineMessages({
  emailRequired: {
    id: 'landing.email.required',
    defaultMessage: 'landing.email.required',
  },
  emailFormat: {
    id: 'landing.email.format',
    defaultMessage: 'landing.email.format',
  },
});

const validations = {
  email: [
    required({
      msg: messages.emailRequired,
    }),
    email({
      msg: messages.emailFormat,
    }),
  ],
};

const validate = (values) => {
  const errors = {};
  // eslint-disable-next-line
  for (const field in validations) {
    const value = values[field];
    errors[field] = validations[field].map(validateField => validateField(value, values)).find(x => x);
  }
  return errors;
};

export default validate;
