process.env.SERVER = 'true';
process.env.CLIENT = 'false';

import env from './env';
import debug from 'debug';
import { createAppServer } from './server/server';
import { connectDatabase } from './server/database';

const initDebug = debug('init');

(async () => {
  let mongooseConnection;
  try {
    mongooseConnection = await connectDatabase();
    initDebug(`Connected to mongo: ${mongooseConnection.host}:${mongooseConnection.port}/${mongooseConnection.name}`);
  } catch (error) {
    initDebug(error);
    process.exit(1);
  }
  const server = await createAppServer(mongooseConnection);

  await server.listen(env.PORT, () => {
    initDebug(`Server listening on localhost:${process.env.PORT}`);
  });
})();
