import { mjml2html } from 'mjml';

const email = (messages) => {

  const template = `
  <mjml>
  <mj-head>
    <mj-font name="OpenSans" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400" />
  </mj-head>
  <mj-body>
    <mj-container background-color="#e5e5e5">
      <mj-section locked="true" padding-bottom="20" padding-top="20">
        <mj-column width="66.66666666666666%" vertical-align="middle">
          <mj-text align="left" color="#000000" font-family="'Open Sans', sans-serif" font-size="11" locked="true" editable="true" padding-left="25" padding-right="25" padding-bottom="0" padding-top="0">
          </mj-text>
        </mj-column>
        <mj-column width="33.33333333333333%" vertical-align="middle">
          <mj-text align="right" color="#000000" font-family="OpenSans, sans-serif" font-size="11" locked="true" editable="false" padding-left="25" padding-right="25" padding-bottom="0" padding-top="0">
          </mj-text>
        </mj-column>
      </mj-section>
      <mj-section background-color="#ffffff" padding-bottom="20" padding-top="20">
        <mj-column width="100%" vertical-align="top">
          <mj-image href="https://powerplace.ch" src="https://powerplace.ch/static/images/logo-powerplace.png" alt="PowerPlace logo" align="center" border="none" width="182" padding-left="0" padding-right="0" padding-bottom="0" padding-top="0">
          </mj-image>
        </mj-column>
      </mj-section>
      
      <mj-section background-color="#04649F" background-url="https://powerplace.ch/static/images/geometric_layer.png" padding-bottom="27" padding-top="0" background-size="cover">
        <mj-column width="100%" vertical-align="top">
          <mj-text align="center" color="#000000" font-family="OpenSans, sans-serif" font-size="13" padding-left="25" padding-right="25" padding-bottom="8" padding-top="40">
            <p><span style="font-weight: bold;"><span style="color: rgb(255, 255, 255);"><span style="font-size: 1.2rem;">${messages.headline}</span></span>
              </span>
            </p>
            <p><span style="font-weight: 300;"><span style="color: rgb(255, 255, 255);"><span style="font-size: 1.2rem;">${messages.headline2}
</span></span>
              </span>
            </p>
          </mj-text>
          <mj-button background-color="#FFFFFF" color="#04649F" font-size="0.8rem" align="center" vertical-align="middle" border="none" padding="15px 30px" border-radius="0px" href="https://powerplace.ch" font-family="OpenSans, sans-serif" padding-left="25" padding-right="25"
            padding-bottom="20" padding-top="15">
            ${messages.buttonText}
          </mj-button>
        </mj-column>
      </mj-section>
      <mj-section background-color="#FFFFFF">
             <mj-group>
        <mj-column width="25%">
          <mj-image href="https://www.facebook.com/PowerPlaceEnergy" height="1.5rem" src="https://powerplace.ch/static/icons/social/facebook_blue.svg" />
        </mj-column>
        <mj-column width="25%">
          <mj-image href="https://www.linkedin.com/company/powerplaceenergy" height="1.5rem;" src="https://powerplace.ch/static/icons/social/linkedin_blue.svg" />
        </mj-column>
        <mj-column width="25%">
          <mj-image href="https://www.instagram.com/powerplaceenergy/" height="1.5rem;" src="https://powerplace.ch/static/icons/social/instagram_blue.svg" />
        </mj-column>
        <mj-column width="25%">
          <mj-image href="https://www.youtube.com/channel/UCeRTAaiPJdEhYMo8VoaEeVg/featured" height="1.5rem;" src="https://powerplace.ch/static/icons/social/youtube_blue.svg" />
        </mj-column>
        </mj-group>
      </mj-section>
 
      <mj-section background-color="#ffffff" padding-bottom="20" padding-top="0">
        <mj-column width="25%" vertical-align="top">

        </mj-column>
        <mj-column width="100%" vertical-align="top">
          <mj-text align="center" color="#000000" font-family="OpenSans, sans-serif" font-size="1.2rem" padding-left="25" padding-right="25" padding-bottom="0" padding-top="0">
            <p style="margin-bottom: 1.5rem; font-size: 1.2rem; color: #04649F; font-weight: bold;">
              ${messages.thankYou}
            </p>
            <p style="margin-bottom: 1.5rem; font-size: 1.2rem; color: #04649F; font-weight: 500;">
              ${messages.powerplaceWorld}
            </p>
            <p style="margin-bottom: 1.5rem; font-size: 1rem; color: #04649F; font-weight: 500;">
              ${messages.weInformYou}
            </p>
            <p style="font-size: 0; color: #04649F; font-weight: 600; white-space:collapse;">
              <span style="font-size: 1rem; color: #04649F;">#${messages.we}</span>
              <span style="font-size: 1rem; color: #555657;">${messages.gift}</span>
              <span style="font-size: 1rem; color: #04649F;">${messages.you}</span>
              <span style="font-size: 1rem; color: #555657;">${messages.time}</span>
            </p>
          </mj-text>
        </mj-column>
      </mj-section>
           
    </mj-container>
  </mj-body>
</mjml>
  `
  const output = mjml2html(template);

  return output;
};

export default email;
