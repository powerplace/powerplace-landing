/**
 * Created by dariodifazio on 16.02.18.
 */
const messages = {
  headline: 'Die intelligente Heizöl Preisvergleichsplattform',
  headline2: 'Vergleiche die Preise und wähle die beste Lösung für deine Bedürfnisse',
  buttonText: 'PowerPlace.ch',
  thankYou: 'Vielen Dank für deine Registrierung!',
  powerplaceWorld: 'Möchtest du Teil der PowerPlace Welt sein?',
  weInformYou: 'Wir werden dich per E-Mail gerne benachrichtigen, wenn unsere Plattform bereit ist.',
  we: 'Wir',
  gift: 'Schenken',
  you: 'Dir',
  time: 'Zeit',
}

export default messages;
