/**
 * Created by dariodifazio on 16.02.18.
 */
const messages = {
  headline: 'La plate-forme intelligente de comparaison de prix pour le mazout',
  headline2: 'Compare les prix et choisis la solution adaptée à tes besoins',
  buttonText: 'PowerPlace.ch',
  thankYou: 'Merci pour ton inscription!',
  powerplaceWorld: 'Tu veux faire partie du monde PowerPlace?',
  weInformYou: "Nous serons heureux de t'informer par e-mail dès l’ouverture de notre plate-forme de comparaison pour le mazout.",
  we: 'NousTe',
  gift: 'Donnons',
  you: 'Du',
  time: 'Temps',
}

export default messages;
