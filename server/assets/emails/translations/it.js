/**
 * Created by dariodifazio on 16.02.18.
 */
const messages = {
  headline: 'La piattaforma intelligente di comparazione prezzi per l’olio combustibile ',
  headline2: 'Confronta i prezzi e scegli la soluzione perfetta per le tue esigenze',
  buttonText: 'PowerPlace.ch',
  thankYou: 'Grazie per la tua e-mail!',
  powerplaceWorld: 'Vuoi entrare a far parte del mondo PowerPlace?',
  weInformYou: "Ti avviseremo con un'e-mail quando la nostra piattaforma di comparazione per l’olio combustibile sarà pronta!",
  we: 'Il',
  gift: 'Tempo',
  you: 'ÈIlNostro',
  time: 'Regalo',
}

export default messages;
