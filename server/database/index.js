/* eslint-disable import/prefer-default-export */
import debug from 'debug';
import mongoose from 'mongoose';
import env from '../../env';


export function connectDatabase() {
  const MONGO_URL = env.MONGO_URL;

  return new Promise((resolve, reject) => {
    mongoose.Promise = global.Promise;

    mongoose.set('debug', env.MONGOOSE_DEBUG);

    mongoose.connection
      .on('error', error => reject(error))
      .on('close', () => console.log('Database connection closed.'))
      .once('open', () => resolve(mongoose.connections[0]));

    mongoose.connect(MONGO_URL, { useMongoClient: true });
  });
}
