import mongoose, { Schema } from 'mongoose';
import { isEmail } from 'validator';
import jwt from 'jsonwebtoken';
import env from '../../../env';

import CommonPlugin from '../plugins/common';

const SubscriptionSchema = new Schema({
  email: {
    type: String,
    required: true,
    index: true,
    trim: true,
    lowercase: true,
    unique: true,
    validate: val => isEmail(val),
  },
  isVerified: {
    type: Boolean,
    default: false,
  },
  verificationToken: {
    type: String,
    trim: true,
  },
});

SubscriptionSchema.plugin(new CommonPlugin());

SubscriptionSchema.method('verificationRequest', async function resetPasswordRequest() {
  const model = this;
  model.verificationToken = jwt.sign({ email: model.email }, env.VERIFICATION_JWT_SECRET, {
    subject: model.id,
  });
  return model.save();
});

SubscriptionSchema.method('consumeVerificationToken', async function consumeResetPasswordToken() {
  const model = this;
  model.verificationToken = undefined;
  return model.save();
});

let Subscription;

try {
  Subscription = mongoose.model('Subscription');
} catch (error) {
  Subscription = mongoose.model('Subscription', SubscriptionSchema);
}

export default Subscription;
