import jsonSelect from 'mongoose-json-select';
import beautifulValidation from 'mongoose-beautiful-unique-validation';
import mongooseTimestamp from 'mongoose-timestamp';

const omitCommonFields = ['_id', '__v'];

export default function CommonPlugin() {
  return function (Schema) {
    Schema.add({
      id: {
        type: String,
        required: true,
        index: true,
        unique: true,
      },
    });

    Schema.pre('validate', function (next) {
      this.id = this._id.toString();
      next();
    });

    Schema.virtual('locale')
      .get(function () {
        return this.__locale;
      })
      .set(function (locale) {
        this.__locale = locale;
      });

    Schema.plugin(mongooseTimestamp, {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    });

    Schema.plugin(
      jsonSelect,
      omitCommonFields.map(field => `-${field}`).join(' '),
    );

    Schema.plugin(beautifulValidation);

    return Schema;
  };
}
