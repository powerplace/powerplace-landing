import { createError, isInstance } from 'apollo-errors';
import { createResolver } from 'apollo-resolvers';

const UnknownError = createError('UnknownError', {
  message: 'An unknown error has occurred!  Please try again later',
});

const baseResolver = createResolver(
  // incoming requests will pass through this resolver like a no-op
  null,

  /*
   Only mask outgoing errors that aren't already apollo-errors,
   such as ORM errors etc
   */
  // (root, args, context, error) => (isInstance(error) ? error : new UnknownError()),
  (root, args, context, error) => (isInstance(error) ? error : new UnknownError()),
);

export default baseResolver;
