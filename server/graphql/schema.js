import { makeExecutableSchema } from 'graphql-tools';

import merge from 'lodash.merge';

import SubscriptionSchema from './subscriptions/schema';

import SubscriptionResolvers from './subscriptions/resolvers';

const rootQuery = `
type Query {
    dummy: Int
}
`;

const rootMutation = `
type Mutation {
    dummy: Int
}
`;

const rootSchema = `
schema {
    query: Query
    mutation: Mutation
}
`;

const schema = makeExecutableSchema({
  typeDefs: [
    rootSchema,
    rootQuery,
    rootMutation,
    SubscriptionSchema,
  ],
  resolvers: merge(SubscriptionResolvers),
});

export default schema;
