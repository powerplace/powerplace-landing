import { createError } from 'apollo-errors';

export const EmailAlreadyInUse = createError(
  'EmailAlreadyInUse',
  {
    message: 'The given email is already in use.',
  },
);

export const TokenError = createError(
  'TokenError',
  {
    message: 'The given token is not valid',
  },
);

export const VerificationError = createError(
  'VerificationError',
  {
    message: 'Could not verify email',
  },
);
