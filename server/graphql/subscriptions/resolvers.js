import jwt, { JsonWebTokenError } from 'jsonwebtoken';
import nodemailer from 'nodemailer';
import emailUtil from '../../assets/emails/email-new';
import baseResolver from '../base/baseResolver';
import {
  EmailAlreadyInUse,
  TokenError,
  VerificationError,
} from './errors';

import deMessages from '../../assets/emails/translations/de';
import frMessages from '../../assets/emails/translations/fr';
import itMessages from '../../assets/emails/translations/it';


const messages = {
  de: deMessages,
  it: itMessages,
  fr: frMessages,
};

export default {
  Query: {
    subscriptionFindMany: baseResolver.createResolver(async (root, {}, { subscriptionModel  }) => {
      return await subscriptionModel.find();
    })
  },
  Mutation: {
    subscriptionCreate: baseResolver.createResolver(async (root, { email }, { subscriptionModel, locale }) => {
      const subscription = await subscriptionModel.findOne({ email });

      if (subscription) throw new EmailAlreadyInUse();

      const newSubscription = await subscriptionModel.create({ email });


      const currentMessages = messages[locale];


      const poolConfig = {
        pool: true,
        host: 'asmtp.mail.hostpoint.ch',
        port: 465,
        secure: true, // use TLS
        auth: {
          user: 'daniele.vatri@powerplace.ch',
          pass: 'sZjL8pAk',
        },
      };

      const emailMessage = emailUtil(currentMessages);

      const message = {
        from: '"PowerPlace" <contact@powerplace.ch>',
        to: email,
        subject: currentMessages.thankYou,
        html: emailMessage.html,
      };

      const transporter = nodemailer.createTransport(poolConfig);

      transporter.sendMail(message, (err) => {
        if (err) {
          console.log(err);
        }
      });

      return newSubscription;
    }),
    subscriptionRemove: baseResolver.createResolver(async (root, { email }, { subscriptionModel, locale }) => {
      const subscription = await subscriptionModel.findOne({ email });
      await subscriptionModel.remove({ email });

      return subscription;
    }),
    subscriptionVerify: baseResolver.createResolver(async (root, { verificationToken }, { subscriptionModel }) => {
      try {
        await jwt.verify(verificationToken, process.env.VERIFICATION_JWT_SECRET);
      } catch (error) {
        if (error instanceof JsonWebTokenError) {
          throw new TokenError();
        }
        throw error;
      }
      const subscription = await subscriptionModel.findOne({ verificationToken });

      if (!subscription) throw new VerificationError();

      return subscription.consumeVerificationToken();
    }),
  },
};
