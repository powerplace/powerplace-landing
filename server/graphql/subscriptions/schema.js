const SubscriptionSchema = `
type Subscription {
    id: ID!
    email: String!
    isVerified: Boolean!,
    verificationToken: String,
    created_at: String
}

extend type Query {
  subscriptionFindMany: [Subscription]
}

extend type Mutation {
    subscriptionCreate(email: String!): Subscription
    subscriptionRemove(email: String!): Subscription
    subscriptionVerify(verificationToken: String!): Subscription
}
`;

export default () => [SubscriptionSchema];
