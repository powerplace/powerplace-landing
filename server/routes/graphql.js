import {
  createError,
  formatError as apolloFormatError,
} from 'apollo-errors';
import accepts from 'accepts';
import {
  graphiqlExpress,
  graphqlExpress,
} from 'apollo-server-express';
import { Router } from 'express';
import { GraphQLError } from 'graphql';
import subscriptionModel from '../database/models/subscription';
import schema from '../graphql/schema';


const UnknownError = createError('UnknownError', {
  message: 'An unknown error has occurred.  Please try again later',
});

const formatError = (error) => {
  let e = apolloFormatError(error);

  if (e instanceof GraphQLError) {
    e = apolloFormatError(new UnknownError({
      data: {
        originalMessage: e.message,
        originalError: e.name,
      },
    }));
  }

  return e;
};

const context = {
  subscriptionModel,
};

const graphqlRouter = new Router();


graphqlRouter.use('/graphql', graphqlExpress(async (request, response) => {
  const accept = accepts(request);
  let locale = accept.language(['de', 'fr', 'it']);
  const cookieLang = request.cookies['powerplace-language'];

  if (cookieLang) {
    locale = `${cookieLang}-CH`;
  }

  locale = locale || 'de-CH';

  return {
    schema,
    formatError,
    context: {
      ...context,
      locale: locale.split('-')[0],
      request,
      response,
    },
  };
}));

graphqlRouter.use('/graphiql', graphiqlExpress({
  endpointURL: '/api/graphql',
}));

export default graphqlRouter;
