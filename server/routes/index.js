import { Router } from 'express';
import graphqlRouter from './graphql';

const router = new Router();

router.use('/', graphqlRouter);

export default router;
