/* eslint-disable import/prefer-default-export */
import config from '../config';
import 'isomorphic-fetch';
import IntlPolyfill from 'intl';
import next from 'next';
import accepts from 'accepts';

import path from 'path';
import express from 'express';
import expressPinoLogger from 'express-pino-logger';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import compression from 'compression';
import serveFavicon from 'serve-favicon';
import serveStatic from 'serve-static';
import methodOverride from 'method-override';
import helmet from 'helmet';
import cors from 'cors';
import expressErrorHandler from 'express-error-handler';


import createServer from '../lib/create-server';
import routes from './routes';
import nextRoutes from '../routes';
import { getLocaleDataScript, getLocaleMessages } from '../lib/locale';

// Polyfill Node with `Intl` that has data for all locales.
// See: https://formatjs.io/guides/runtime-environments/#server
global.Intl.NumberFormat = IntlPolyfill.NumberFormat;
global.Intl.DateTimeFormat = IntlPolyfill.DateTimeFormat;

const dev = config.nodeEnv !== 'production';


export async function createAppServer(mongooseConnection) {
  const nextApp = next({ dev });

  const handler = nextRoutes.getRequestHandler(nextApp);
  await nextApp.prepare();

  const app = express();


  const server = createServer({
    cert: config.sslCertificate,
    key: config.sslCertificateKey,
  }, app);


  app.disable('x-powered-by');

  app.use(expressPinoLogger());
  app.use(methodOverride());
  app.use(cookieParser());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(compression());
  app.use(serveFavicon(path.join(process.cwd(), 'static', 'favicon.ico')));
  app.use(serveStatic(path.join(process.cwd(), 'static')));
  app.use(helmet());
  app.use(cors());

  app.use(expressErrorHandler({ server }));

  app.use('/api', routes);

  app.get('*', (req, res) => {
    const accept = accepts(req);
    let locale = accept.language(['de', 'fr', 'it']);
    const cookieLang = req.cookies['powerplace-language'];

    if (cookieLang) {
      locale = `${cookieLang}-CH`;
    }

    locale = locale || 'de-CH';

    req.locale = locale
    req.localeDataScript = getLocaleDataScript(locale);
    req.messages = getLocaleMessages(locale.split('-')[0]);
    if (!req.path.includes('_next') && !req.path.includes('static')) {
      res.setHeader('content-type', 'text/html; charset=utf-8');
    }

    return handler(req, res);
  });

  app.use((req, res) => {
    res.statusCode = 200;
  });

  app.use((req, res) => handler(req, res));

  return server;
}

