import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../redux/root.reducer';

export const initStore = (initialState, isServer) => {
  if (isServer && typeof window === 'undefined') {
    return createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)));
  }
  if (!window.store) {
    window.store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)));
  }
  return window.store;
};
